from flask import Blueprint,render_template, request, redirect, url_for
from flask_login import LoginManager, login_user, logout_user, login_required, current_user
import requests,sys,json
from RequetsUtil import RequestUtil,HttpMethod
from User import User
from werkzeug.security import generate_password_hash
from urllib.error import HTTPError

url_for_image = "https://image.tmdb.org/t/p/w220_and_h330_face"
URL = "http://api:5000/api/"
myApiRequest = RequestUtil(base_url=URL)
dashboard_bp = Blueprint('dashboard', __name__)

@dashboard_bp.route('/manual_add_movie', methods=['GET','POST'])
@login_required
def manual_add_movie():
    if request.method == 'POST':
        title = request.form['title']
        overview = request.form['overview']
        duration = request.form['duration']
        release_date = request.form['release_date']
        genre = request.form['genre']
        director = request.form['director']
        actors = request.form.getlist('actors[]')
        rating = request.form['rating']
        support = request.form['support']
        parametersToSend = {
            'user_id' : current_user.id,
            'title': title,
            'overview': overview,
            'duration': duration,
            'release_date' : release_date,
            'genre' : genre,
            'director' : director,
            'actors' : actors,
            'rating': rating,
            'support' : support
        }
        try:
            response_data_movies = myApiRequest.make_request(method=HttpMethod.POST,endpoint='manual_add_movie',data=parametersToSend)
            return redirect(url_for('dashboard'))
        except Exception as e:
            return render_template('error.html', error_message="Une erreur s'est produite lors de l'ajout du film.")
        
    
    return render_template('formManualAddMovie.html')

@dashboard_bp.route('/search_movie', methods=['GET'])
@login_required
def search_movie():
    query = str(request.args.get('search'))
    parameters = {'user_id' : current_user.id,'search': query}
    response_data_movies = myApiRequest.make_request(method=HttpMethod.POST,endpoint='tmdb',data=parameters)
    data_movies = response_data_movies.json()
    return render_template('printSearchMovie.html', data_movies=data_movies,url_for_image=url_for_image)

@dashboard_bp.route('/add_movie/<int:movie_id>', methods=['GET'])
@login_required
def add_movie(movie_id):
    parameters = {'user_id' : current_user.id,'movie_id': movie_id}
    try:
        response_add_movies = myApiRequest.make_request(method=HttpMethod.POST, endpoint='add_movie', data=parameters)
        return redirect(url_for('dashboard'))

    except Exception as e:
        return render_template('error.html', error_message="Une erreur s'est produite lors de l'ajout du film.")

@dashboard_bp.route('/view_info_movie/<int:movie_id>', methods=['GET'])
@login_required
def view_info_movie(movie_id):
    parameters = {
        'user_id' : current_user.id,
        'movie_id': movie_id
    }
    try:
        response_data_movies = myApiRequest.make_request(method=HttpMethod.POST, endpoint='info_movie', data=parameters)
        data_movies = response_data_movies.json()
        return render_template('infoMovie.html', theMovie=data_movies)
    except Exception as e:
        return render_template('error.html', error_message=f"Le film n'existe pas. Raison: {e} | Type: {type(data_movies)}/ Requete: {data_movies}")
    
@dashboard_bp.route('/update_movie/<int:movie_id>', methods=['GET','POST'])
@login_required
def update_movie(movie_id):
    if request.method == 'POST':
        movie_id = request.form['movie_id']
        title = request.form['title']
        overview = request.form['overview']
        duration = request.form['duration']
        release_date = request.form['release_date']
        genre = request.form['genre']
        director = request.form['director']
        actors = request.form.getlist('actors[]')
        support = request.form['support']
        poster_path = request.form['poster_path']
        try:
            rating = request.form['rating']
        except KeyError:
            rating = request.form['rating_basic']
        parametersToSend = {
            'movie_id': movie_id,
            'user_id' : current_user.id,
            'title': title,
            'overview': overview,
            'duration': duration,
            'release_date' : release_date,
            'genre' : genre,
            'director' : director,
            'actors' : actors,
            'rating': rating,
            'support' : support,
            'poster_path' : poster_path
        }
        try:
            response_data_movies = myApiRequest.make_request(method=HttpMethod.POST,endpoint='update',data=parametersToSend)
            return redirect(url_for('dashboard'))
        except Exception as e:
            return render_template('error.html', error_message="Une erreur s'est produite lors de la modification du film.")
    else:
        parameters = {'user_id' : current_user.id,'movie_id': movie_id}
        try:
            response_update_movie = myApiRequest.make_request(method=HttpMethod.POST, endpoint='info_movie', data=parameters)
            data_movie = response_update_movie.json()
            return render_template('formUpdateMovie.html', data_movie=data_movie)

        except Exception as e:
            return render_template('error.html', error_message=f"Une erreur s'est produite lors de la demande de modification du film. Erreur: {e} / {data_movie}")
        
@dashboard_bp.route('/delete_movie/<int:movie_id>', methods=['GET'])
@login_required
def delete_movie(movie_id):
    parameters = {
        'user_id' : current_user.id,
        'movie_id': movie_id
    }
    try:
        response_data_movies = myApiRequest.make_request(method=HttpMethod.POST, endpoint='delete', data=parameters)
        data_movies = response_data_movies.json()
        return redirect(url_for('dashboard'))
    except Exception as e:
        return render_template('error.html', error_message=f"Le film n'existe pas. Raison: {e} | movie {data_movies}")