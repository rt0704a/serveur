from flask import Blueprint,render_template, request, redirect, url_for
from flask_login import LoginManager, login_user, logout_user, login_required, current_user
import requests,sys,json
from RequetsUtil import RequestUtil,HttpMethod
from User import User
from werkzeug.security import generate_password_hash

URL = "http://api:5000/"
url_auth = "user/"
myApiRequest = RequestUtil(base_url=URL)
auth_bp = Blueprint('auth', __name__)

#Fonction permettant l'inscription
@auth_bp.route('/signup', methods=['POST','GET'])
def signup():
    # Vérifiez si l'utilisateur est déjà connecté
    if current_user.is_authenticated:
        return redirect(url_for('dashboard'))
    if request.method == 'POST':
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        email = request.form['email']
        password = request.form['password']
        hashed_password = generate_password_hash(password, method='scrypt')

        #Dans un premier temps envoie de l'email à l'api pour savoir s'il existe déjà dans le fichier
        data_email = {'email' : email}

        response_email = myApiRequest.make_request(method=HttpMethod.POST, endpoint=f'{url_auth}check_email', data=data_email)
        result_email = response_email.json()
        if result_email['email_already_use'] is True:
            return render_template('signup.html', is_email_already_use=True)
        
        #Si l'email n'existe pas alors on envoie toutes les données
        elif result_email['email_already_use'] is False:
            data_signup = {'first_name': first_name, 'last_name':last_name, 'email' : email, 'password': hashed_password}
            response_signup = myApiRequest.make_request(method=HttpMethod.POST, endpoint=f'{url_auth}signup_user', data=data_signup)
            result = response_signup.json()
            if 'user_id' in result and result['user_id'] is not None:
                user_id = result['user_id']
                user = User(user_id,first_name,last_name,email,hashed_password)
                login_user(user)
                return redirect(url_for('dashboard'))
            else:
                raise ValueError("Erreur: Impossible de récupérer l'user_id.")
        else: 
            raise ValueError("Erreur: Impossible de récupérer l'adresse mail")
    else:
        return render_template('signup.html')

#Fonction permettant à l'utilisateur de se connecter
@auth_bp.route('/login', methods=['POST','GET'])
def login():
    # Vérifiez si l'utilisateur est déjà connecté
    if current_user.is_authenticated:
        return redirect(url_for('dashboard'))
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        data_login = {'email': email, 'password':password}
        #Envoi des informations de l'utilisateur à l'api pour la consultation des fichiers JSON
        response_login = myApiRequest.make_request(method=HttpMethod.POST, endpoint=f'{url_auth}login_user', data=data_login)
        result = response_login.json()
        #Si login correct on renvoie l'utilisateur sur son dashboard sinon il doit le resaisir
        if result['login_is_correct'] is True:
            user_id = result['user_id']
            first_name = result['first_name']
            last_name = result['last_name']
            hashed_password = result['password']
            user = User(user_id,first_name,last_name,email,hashed_password)
            login_user(user)
            return redirect(url_for('dashboard'))
        else:
            return render_template('login.html', login_is_correct=False)
    else:
        return render_template('login.html')

#Fonction permettant à l'utilisateur de se déconnecter
@auth_bp.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))