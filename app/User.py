from flask_login import UserMixin

class User(UserMixin):
    def __init__(self, user_id, first_name, last_name, email, password):
        self.id = str(user_id)
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.password = password

    @property
    def is_authenticated(self):
        return True

    def get_id(self):
        return str(self.id)
