from flask import Flask, render_template, request, redirect, url_for
from flask_login import LoginManager, login_user, logout_user, login_required, current_user
import requests,sys,json
from RequetsUtil import RequestUtil,HttpMethod
from User import User
from auth import auth_bp,url_auth
from dashboard import dashboard_bp
from werkzeug.security import generate_password_hash

URL = "http://api:5000/"
myApiRequest = RequestUtil(base_url=URL)

app = Flask(__name__)
app.register_blueprint(auth_bp, url_prefix='/auth')
app.register_blueprint(dashboard_bp, url_prefix='/dashboard')
app.config['SECRET_KEY'] = 'ceci_est_une_clé_secrète_pas_très_secrète'
login_manager = LoginManager(app)

# Fonction de chargement d'utilisateur pour Flask-Login
@login_manager.user_loader
def load_user(user_id):
    data_user = {'user_id': user_id}
    response_user = myApiRequest.make_request(method=HttpMethod.POST, endpoint=f'{url_auth}get_user', data=data_user)

    # Vérifiez si la requête a réussi et retournez l'objet User si c'est le cas
    if response_user.status_code == 200:
        user_data = response_user.json()
        user = User(user_id, user_data['first_name'], user_data['last_name'], user_data['email'], user_data['password'])
        return user
    
    # Si la requête échoue ou l'utilisateur n'est pas trouvé, retournez None
    return None

@app.route('/')
def index():
    # Vérifiez si l'utilisateur est déjà connecté
    if current_user.is_authenticated:
        return redirect(url_for('dashboard'))
    return render_template('index.html')

#Fonction permettant à l'utilisateur d'accéder à sa vidéothèque
@app.route('/dashboard')
#Protection permettant d'empêcher un utilisateur non connecté d'accéder au dashboard
@login_required
def dashboard():
    #Charger les films correspondants à l'id de l'utilisateur connecté
    parameters = {'user_id' : current_user.id}
    response_data_movies = myApiRequest.make_request(method=HttpMethod.POST,endpoint='user/show_videotheque',data=parameters)
    data_movies = response_data_movies.json()
    return render_template('dashboard.html',first_name=current_user.first_name, last_name=current_user.last_name, movies_data=data_movies)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
